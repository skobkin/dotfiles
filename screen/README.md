# GNU Screen configuration

## Install

Create symbolic link for main config file:

```bash
# Suppose your current machine is 'desktop'
ln -s ~/.dotfiles/screen/desktop.screenrc ~/.screenrc
```
