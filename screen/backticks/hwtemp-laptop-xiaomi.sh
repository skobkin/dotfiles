#!/usr/bin/env bash
# Xiaomi Notebook Air 13.3 (2016)
sensors | egrep 'Core [0-9]\:' | awk '{print $3}' | tr "\n" " " | sed -e 's/\ $//'

