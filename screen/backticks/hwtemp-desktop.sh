#!/usr/bin/env bash
# Core i7-4820k
#sensors | fgrep -i "core " | tr "\n" " " | sed -e "s/Core\ [0-9]\:[ ]\++//g;s/  ([^)]*)//g;s/ $//"
# Threadripper 1950X (via motherboard sensors, not die)
sensors | fgrep 'Tdie:' | awk '{print $2}' | head -n1
