#!/usr/bin/env bash
show_time () {
    date -d@${1} -u '+%H:%M';
}

# Xiaomi Notebook Air 13.3 (2016)
CAPACITY=`cat /sys/class/power_supply/BAT0/capacity`
STATUS=`cat /sys/class/power_supply/BAT0/status`
CURRENT=`cat /sys/class/power_supply/BAT0/current_now`
CHARGE=`cat /sys/class/power_supply/BAT0/charge_now`
ESTIMATE_SEC=`echo "(${CHARGE} * 3600) / ${CURRENT}" | bc -l`
ESTIMATE_SEC_INT=${ESTIMATE_SEC%.*}
ESTIMATE_TEXT=`show_time $ESTIMATE_SEC_INT`

# Printing STATUS, CAPACITY, TIME ESTIMATE
echo "${STATUS:0:1} ${CAPACITY}% (${ESTIMATE_TEXT})"
