#!/usr/bin/env bash
free -h | fgrep -i 'mem:' | awk '{print $3, "/", $2}'